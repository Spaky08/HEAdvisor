describe('Testing e2e test', function () {
	var ptor;

	beforeEach(function() {
		ptor = protractor.getInstance();
    	ptor.get('/');
    }); 

    it ('should redirect to /reports when / is accessed', function() {
    	expect(ptor.getCurrentUrl()).toContain('/reports');
    }); 

	it ('should accept /categories', function() {
		ptor.get('#/categories');
		expect(ptor.getCurrentUrl()).toContain('/categories');
	})
});