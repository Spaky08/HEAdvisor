describe('Midway: Testing module', function () 
{

    it('should check that e2e tests are working', function () 
    {
        expect(1).toEqual(1);
    });

    describe("App Module:", function() 
    {
        var module;

        beforeEach(function() 
        {
          module = angular.module("myApp");
        });

        it ("should be registered", function () 
        {
        	expect(module).not.toBeNull();
        });

        describe("Dependencies", function() 
        {
          var deps;
          var hasModule = function(m) 
          {
          	return deps.indexOf(m) >= 0;
          };

          beforeEach(function() 
          {
          	deps = module.value('appName').requires;
          });

          it("should have dependency - ngResource", function() 
          {
          	expect(hasModule('ngResource')).toEqual(true);	           
		      });

          it("should have dependency - ngRoute", function() 
          {
          	expect(hasModule('ngRoute')).toEqual(true);	           
		      });    	
        });
    });
});

describe("Testing Routes", function() {

  var test, onChange;

  beforeEach (function ()
  {
    ngMidwayTester.register('myApp', function(instance)
    {
      test = instance;
      test.$rootScope.$on('$routeChangeSuccess', function()
      {
        if (onChange) onChange();
      });
    });
  });

  it('should map routes to controllers', function() 
  {
    module('myApp');

    inject(function($route) 
    {

      expect($route.routes['/categories'].controller).toBe('CategoryController');
      expect($route.routes['/categories'].templateUrl).
        toEqual('resources/view/category/categoryList.html');
    });
  });

  it("should goto the categories path by default", function() 
  {
    onChange = function()
    {
      expect(test.view().innerHTML).toContain('All categories');
    };
    test.path('/');
  });
});