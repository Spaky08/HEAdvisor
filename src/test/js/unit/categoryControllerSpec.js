describe('Testing category controller', function() {
    var $scope = null;
    var ctrl = null;
    var emptyCategory = {id: 0,name: "",type: "",amount: 0};
    var inputCategory = {id:3, name:"cat3", type:"fixed", amount: 300};

    var ADD_NEW_CATEGORY_TITLE = "Add New Category";
    var UPDATE_CATEGORY_TITLE = "Upadte Category";

    /* A mocked version of our service, someService
     * we're mocking this so we have total control and we're
     * testing this in isolation from any calls it might
     * be making.
     */
    var mockCategoriesService = {

        getCategoryResource: {
            remove: function(){ return true; },
            save: function() { return true; },
            query: function () {
                return [
                    { id: 1, name: "cat1", type: "income", amount: 100},
                    { id: 2, name: "cat2", type: "income", amount: 200}
                ]
            }
        }
    };

    var mockHttp = {
        get: function() {
            return {
                success: function (callback){
                    callback([ "income", "fixed" ]);
                }
            }
        }
    };
    //you need to indicate your module in a test
    beforeEach(module('myApp'));

    /* IMPORTANT!
     * this is where we're setting up the $scope and
     * calling the controller function on it, injecting
     * all the important bits, like our mockService */
    beforeEach(inject(function($rootScope, $controller) {
        //create a scope object for us to use.
        $scope = $rootScope.$new();

        //now run that scope through the controller function,
        //injecting any services or other injectables we need.
        ctrl = $controller('CategoryController', {
            $scope: $scope,
            $http: mockHttp,
            Categories: mockCategoriesService
        });
    }));

    it ("should check initial scope state", function (){

        // check all initial data sets:
        expect($scope.data.categories).toEqual([]);
        expect($scope.data.categoryTypes).toEqual([ "income", "fixed" ]);
        expect($scope.data.orderBy).toEqual('name');
        expect($scope.data.reverse).toEqual(false);

        expect($scope.elementClass.name).toEqual('fa fa-sort-up');
        expect($scope.elementClass.amount).toEqual('fa fa-sort');
        expect($scope.elementClass.type).toEqual('fa fa-sort');

        expect($scope.auCategory.hideEditOrAdd).toEqual(true);
        expect($scope.auCategory.tempCategory.type).toEqual('income');
        expect($scope.auCategory.chosenCategory).toEqual(emptyCategory);

    });

    it ("should add Category", function () {
        $scope.addCategory();

        expect($scope.auCategory.title).toEqual(ADD_NEW_CATEGORY_TITLE);
    });

    it("should edit category", function () {

        $scope.editCategory(inputCategory);

        expect($scope.auCategory.hideEditOrAdd).toBeFalsy();
        expect($scope.auCategory.tempCategory).toEqual(inputCategory);
        expect($scope.auCategory.chosenCategory).toEqual(inputCategory);
        expect($scope.auCategory.title).toEqual(UPDATE_CATEGORY_TITLE);
    });

    it("should call remove on Category service", function () {
        spyOn(mockCategoriesService.getCategoryResource, 'remove').andCallThrough();

        $scope.deleteCategory(inputCategory);
        expect(mockCategoriesService.getCategoryResource.remove).toHaveBeenCalled();
    });

    it("should call save on Category service", function () {
        spyOn(mockCategoriesService.getCategoryResource, 'save').andCallThrough();

        $scope.saveCategory(inputCategory);
        expect(mockCategoriesService.getCategoryResource.save).toHaveBeenCalled();
    });

    it("should reset category", function () {
        $scope.auCategory.tempCategory = angular.copy(inputCategory);
        $scope.auCategory.chosenCategory = inputCategory;
        expect($scope.auCategory.tempCategory).toEqual($scope.auCategory.chosenCategory);

        $scope.auCategory.tempCategory.amount = 800;

        expect($scope.auCategory.tempCategory).not.toEqual($scope.auCategory.chosenCategory);

        $scope.resetCategory();
        expect($scope.auCategory.tempCategory).toEqual(emptyCategory);
    });

    it("should set orderBy", function () {
        expect($scope.data.orderBy).toEqual('name');
        expect($scope.data.reverse).toBeFalsy();
        expect($scope.elementClass.name).toEqual('fa fa-sort-up');
        expect($scope.elementClass.amount).toEqual('fa fa-sort');

        $scope.setOrderBy('name');
        expect($scope.data.orderBy).toEqual('name');
        expect($scope.data.reverse).toBeTruthy();
        expect($scope.elementClass.name).toEqual('fa fa-sort-down');
        expect($scope.elementClass.amount).toEqual('fa fa-sort');

        $scope.setOrderBy('amount');
        expect($scope.data.orderBy).toEqual('amount');
        expect($scope.data.reverse).toBeFalsy();
        expect($scope.elementClass.amount).toEqual('fa fa-sort-up');
        expect($scope.elementClass.name).toEqual('fa fa-sort');
    });

});
