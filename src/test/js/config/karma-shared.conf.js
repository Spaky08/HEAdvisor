var shared = function(config) {
    config.set({
        basePath: '../../..',        
        reporters: ['progress'],
        browsers: ['Chrome'],
        autoWatch: true,
        logLevel: config.LOG_INFO,

        // these are default values anyway
        singleRun: false,
        colors: true
    });
};

shared.files = [
    'main/webapp/resources/lib/angular/angular.js',
    'main/webapp/resources/lib/angular/angular-resource.js',
    'main/webapp/resources/lib/angular/angular-route.js',
    'main/webapp/resources/js/controllers/reportController.js',
    'main/webapp/resources/js/**/*.js',
    'main/webapp/resources/js/app.js',
    'test/js/lib/angular/angular-mocks.js'
];

module.exports = shared;