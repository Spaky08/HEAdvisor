/**
 * Created with IntelliJ IDEA.
 * User: Eliezer
 * Date: 27/10/13
 * Time: 23:45
 * To change this template use File | Settings | File Templates.
 */
var shared = require('./karma-shared.conf');

module.exports = function(config) {
    shared(config);

    config.frameworks = ['jasmine'];
    config.files = shared.files.concat([
        //extra testing code
        'test/js/lib/angular/angular-mocks.js',

        //test files
        'test/js/unit/**/*.js'
    ]);
};