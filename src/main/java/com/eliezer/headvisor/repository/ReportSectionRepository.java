package com.eliezer.headvisor.repository;

import com.eliezer.headvisor.model.ReportSection;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportSectionRepository extends JpaRepository<ReportSection, Integer> {
}
