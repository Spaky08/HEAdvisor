package com.eliezer.headvisor.repository;

import com.eliezer.headvisor.model.TransactionMethod;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionMethodRepository extends JpaRepository<TransactionMethod, Integer> {
}
