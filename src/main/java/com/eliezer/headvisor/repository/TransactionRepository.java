package com.eliezer.headvisor.repository;

import com.eliezer.headvisor.model.Report;
import com.eliezer.headvisor.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    List<Transaction> findByReport(Report report);
}
