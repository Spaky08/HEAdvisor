package com.eliezer.headvisor.repository;


import com.eliezer.headvisor.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ReportRepository extends JpaRepository<Report, Integer> {
}
