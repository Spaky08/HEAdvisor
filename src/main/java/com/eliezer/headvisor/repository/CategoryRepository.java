package com.eliezer.headvisor.repository;

import com.eliezer.headvisor.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Date: 25/10/13
 */

public interface CategoryRepository extends JpaRepository<Category, Integer>
{
}
