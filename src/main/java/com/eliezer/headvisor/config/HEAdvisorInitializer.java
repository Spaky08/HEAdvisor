package com.eliezer.headvisor.config;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * Date: 27/09/13
 */
public class HEAdvisorInitializer implements WebApplicationInitializer
{
    private static final Class<?>[] basicConfigurationClasses = new Class<?>[] {};

    private static final String DISPATCHER_SERVLET_NAME = "dispatcher";

    @Override
    public void onStartup( ServletContext servletContext ) throws ServletException
    {
        //registerListener( servletContext );
        registerDispatcherServlet(servletContext);
        configureLogger();
    }

    private void registerListener(ServletContext servletContext) {
        AnnotationConfigWebApplicationContext rootContext = createContext(basicConfigurationClasses);
        servletContext.addListener(new ContextLoaderListener(rootContext));
        servletContext.addListener(new RequestContextListener());
    }

    private void registerDispatcherServlet(ServletContext servletContext) {
        AnnotationConfigWebApplicationContext dispatcherContext = createContext(HEAdvisorConfig.class);
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet(DISPATCHER_SERVLET_NAME,
                new DispatcherServlet(dispatcherContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");
    }

    private void configureLogger(){
        String log4jLocation = "E:\\Documents\\Dev\\SpringTut\\HEAdvisor\\src\\main\\webapp\\WEB-INF\\log4j.properties";
        PropertyConfigurator.configure( log4jLocation );

    }

    /**
     * Factory method to create {@link AnnotationConfigWebApplicationContext} instances.
     * @param annotatedClasses - annotated classes
     * @return  AnnotationConfigWebApplicationContext - the app context
     */
    private AnnotationConfigWebApplicationContext createContext(final Class<?>... annotatedClasses) {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(annotatedClasses);
        return context;
    }
}
