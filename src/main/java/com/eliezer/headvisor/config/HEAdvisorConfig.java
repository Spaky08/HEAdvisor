package com.eliezer.headvisor.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * Date: 27/09/13
 */
@Configuration
@EnableWebMvc
@Import(DBConfig.class)
@ComponentScan(basePackages = {"com.eliezer.headvisor","com.eliezer.headvisor.config",
                               "com.eliezer.headvisor.repository", "com.eliezer.headvisor.services" })
public class HEAdvisorConfig extends WebMvcConfigurerAdapter
{
    @Override
    public void addResourceHandlers( ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/**");
    }

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
        internalResourceViewResolver.setOrder(2);
        internalResourceViewResolver.setPrefix("/WEB-INF/pages/");
        internalResourceViewResolver.setSuffix(".jsp");
        internalResourceViewResolver.setViewClass(JstlView.class);
        return internalResourceViewResolver;
    }

/*    @Bean
    public CategoryService categoryService()
    {
        CategoryService categoryService = new CategoryService();
        return categoryService;
    }*/
}
