package com.eliezer.headvisor.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public @Data
class TransactionMethod {
    @Id
    @GeneratedValue
    private Integer id;

    private String name;
    private String description;

}
