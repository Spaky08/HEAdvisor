package com.eliezer.headvisor.model;

/**
 * Date: 21/11/13
 */
public class MoneyTransactions
{

    private Integer           id;
    private double            predictedAmount;
    private double            actualAmount;
    private String            categoryName;
    private String            comments;
    private MoneyTransferType moneyTransferType;

    public Integer getId()
    {
        return id;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }

    public double getPredictedAmount()
    {
        return predictedAmount;
    }

    public void setPredictedAmount( double predictedAmount )
    {
        this.predictedAmount = predictedAmount;
    }

    public double getActualAmount()
    {
        return actualAmount;
    }

    public void setActualAmount( double actualAmount )
    {
        this.actualAmount = actualAmount;
    }

    public MoneyTransferType getOneyTransferType()
    {
        return moneyTransferType;
    }

    public void setOneyTransferType( MoneyTransferType oneyTransferType )
    {
        moneyTransferType = oneyTransferType;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments( String comments )
    {
        this.comments = comments;
    }

    public String getCategoryName()
    {
        return categoryName;
    }

    public void setCategoryName( String categoryName )
    {
        this.categoryName = categoryName;
    }

    public MoneyTransactions()
    {
        id = 0;
        predictedAmount = 0;
        actualAmount = 0;
        categoryName = "";
        comments = "";
        moneyTransferType = MoneyTransferType.CASH;
    }
}
