package com.eliezer.headvisor.model;

import java.util.*;

/**
 * Date: 21/11/13
 */
public class MonthlyReport
{
    private Integer              id;
    private Date                 date;
    private String               comments;
    private Map<String, MonthlyReportSection> reportSections;


    public MonthlyReport()
    {
        this.date = new Date();
        this.comments = "";
        this.reportSections = new HashMap<String, MonthlyReportSection>();
    }

    public Integer getId()
    {
        return id;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate( Date date )
    {
        this.date = date;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments( String comments )
    {
        this.comments = comments;
    }


    public Map<String, MonthlyReportSection> getReportSections() {
        return reportSections;
    }

    public MonthlyReportSection getReportSectionByReportType (String reportType) {
        return this.reportSections.get(reportType);
    }
}
