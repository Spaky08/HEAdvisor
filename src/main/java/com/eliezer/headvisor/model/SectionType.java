package com.eliezer.headvisor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import javax.persistence.Entity;
import java.util.HashMap;

/**
 * Date: 22/11/13
 */
public enum SectionType
{
    INCOME ("Income"),
    FIXED ("Fixed Expense"),
    VAR ("Var Expense"),
    ONE_TIME ("One Time Transaction");

    private String value;

    private SectionType( String value )
    {
        this.value = value;
    }

    public HashMap<String, Object> toMap()
    {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("value", this);
        map.put("displayName", this.toString());
        return map;
    }

    @JsonValue
    public String getValue()
    {
        return this.value;
    }

    @JsonCreator
    public static SectionType create(String val){
        SectionType[] sectionTypes = SectionType.values();
        for (SectionType sectionType: sectionTypes) {
            if (sectionType.getValue().equals( val )){
                return sectionType;
            }
        }
        return sectionTypes[0];
    }


    @Override
    public String toString()
    {
        return this.value;
    }
}
