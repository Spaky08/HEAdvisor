package com.eliezer.headvisor.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Date: 21/11/13
 */
public class MonthlyReportSection
{

    private Integer                      id;
    private Date                         date;
    private ArrayList<MoneyTransactions> moneyTransaction;
    private SectionType        categoryType;

    public Integer getId()
    {
        return id;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate( Date date )
    {
        this.date = date;
    }

    public ArrayList<MoneyTransactions> getOneyTransaction()
    {
        return moneyTransaction;
    }

    public void setOneyTransaction( ArrayList<MoneyTransactions> oneyTransaction )
    {
        moneyTransaction = oneyTransaction;
    }

    public SectionType getCategoryType()
    {
        return categoryType;
    }

    public void setCategoryType( SectionType categoryType )
    {
        this.categoryType = categoryType;
    }

    public MonthlyReportSection(Date date, SectionType categoryType)
    {
        this.date = date;
        this.categoryType = categoryType;
    }


}
