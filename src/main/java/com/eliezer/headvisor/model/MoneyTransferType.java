package com.eliezer.headvisor.model;

/**
 * Date: 21/11/13
 */
public enum MoneyTransferType
{
    CHECK("check"),
    CREDIT("credit"),
    CASH("cash"),
    BANK_TRANSACTION("bank transaction");

    private String value;

    private MoneyTransferType( String value )
    {
        this.value = value;
    }
}
