package com.eliezer.headvisor.model;

import javax.persistence.*;


@Entity
public class Category
{
    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private int amount;

    @ManyToOne
    @JoinColumn(name = "reportSection_id")
    private ReportSection reportSection;

    public Category()
    {
        id = 0;
        name = "";
        amount = 0;
    }

    public Category( String name, int amount, ReportSection reportSection )
    {
        this.name = name;
        this.amount = amount;
        this.reportSection = reportSection;
    }

    public Category( int id, String name, int amount, ReportSection reportSection )
    {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.reportSection = reportSection;
    }

    public int getAmount()
    {
        return amount;
    }


    public void setAmount( int amount )
    {
        this.amount = amount;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public int getId()
    {
        return id;
    }

    public ReportSection getReportSection() {
        return reportSection;
    }

    public void setReportSection(ReportSection reportSection) {
        this.reportSection = reportSection;
    }
}
