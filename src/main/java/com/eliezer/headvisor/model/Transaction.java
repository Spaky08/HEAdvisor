package com.eliezer.headvisor.model;

import lombok.Data;

import javax.persistence.*;


@Entity
public @Data
class Transaction{

    @Id
    @GeneratedValue
    private Integer id;

    private double predictedAmount;
    private double actualAmount;
    private String comments;

    @ManyToOne
    @JoinColumn(name = "report_id")
    private Report report;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "transaction_method_id")
    private TransactionMethod transactionMethod;
}
