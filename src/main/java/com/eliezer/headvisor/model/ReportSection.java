package com.eliezer.headvisor.model;

import javax.persistence.*;

@Entity
public class ReportSection {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;
    private String description;

    @Column(name = "DisplayOrder")
    private int order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public ReportSection() {
        this.name = "";
        this.description = "";
        this.order = 0;
    }

    @Override
    public String toString() {
        return "RequestSection{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", order=" + order +
                '}';
    }
}
