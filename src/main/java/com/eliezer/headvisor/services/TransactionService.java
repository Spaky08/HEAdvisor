package com.eliezer.headvisor.services;

import com.eliezer.headvisor.model.Report;
import com.eliezer.headvisor.model.Transaction;
import com.eliezer.headvisor.repository.ReportRepository;
import com.eliezer.headvisor.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private ReportRepository reportRepository;

    public Transaction createOrUpdateTransaction(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    public List<Transaction> getTransaction() {
        return transactionRepository.findAll();
    }

    public void removeTransaction(Integer id) {
        transactionRepository.delete(id);
    }

    public List<Transaction> getTransactionsByReport(int reportId) {
        Report report = reportRepository.findOne(reportId);
        return transactionRepository.findByReport(report);

    }
}
