package com.eliezer.headvisor.services;

import com.eliezer.headvisor.model.ReportSection;
import com.eliezer.headvisor.repository.ReportSectionRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConfigReportSectionService {

    @Autowired
    private ReportSectionRepository reportSectionRepository;

    private static final Logger logger = Logger.getLogger( CategoryService.class );

    public ReportSection createOrUpdateReportSection(ReportSection reportSection) {
        logger.debug("report section is called with : " + reportSection);

        reportSectionRepository.save(reportSection);

        return reportSection;
    }

    public List<ReportSection> getReportSectionList() {
        logger.debug("report section list is called");
        return reportSectionRepository.findAll ();
    }


    public void removeReportSection(Integer id) {
        reportSectionRepository.delete(id);
    }
}
