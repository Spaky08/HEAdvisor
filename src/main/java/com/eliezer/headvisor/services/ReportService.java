package com.eliezer.headvisor.services;

import com.eliezer.headvisor.model.Report;
import com.eliezer.headvisor.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService {

    @Autowired
    private ReportRepository reportRepository;

    public List<Report> getReportList() {
        return reportRepository.findAll();
    }

    public Report createOrUpdateReport(Report report) {
       return reportRepository.save(report);
    }

    public void removeReport(Integer id) {
        reportRepository.delete(id);
    }

    public Report getReport(Integer id) { return reportRepository.findOne(id); }
}
