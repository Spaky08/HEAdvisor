package com.eliezer.headvisor.services;

import com.eliezer.headvisor.model.TransactionMethod;
import com.eliezer.headvisor.repository.TransactionMethodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionMethodService {

    @Autowired
    private TransactionMethodRepository transactionMethodRepository;

    public TransactionMethod createOrUpdateTransactionMethod(TransactionMethod transactionMethod) {
        return transactionMethodRepository.save(transactionMethod);
    }

    public List<TransactionMethod> getTransactionMethods() {
        return transactionMethodRepository.findAll();
    }

    public void removeTransactionMethod(Integer id) {
        transactionMethodRepository.delete(id);
    }
}
