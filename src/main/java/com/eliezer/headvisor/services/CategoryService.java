package com.eliezer.headvisor.services;

import com.eliezer.headvisor.repository.CategoryRepository;
import com.eliezer.headvisor.model.Category;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



/**
 * Date: 18/10/13
 */
@Service
public class CategoryService
{
    @Autowired
    private CategoryRepository categoryRepository;

    private static final Logger logger = Logger.getLogger( CategoryService.class );

    public CategoryService(){}

    public List<Category> getCategoryList()
    {
        logger.debug("getCategoryList called");
        return categoryRepository.findAll ();
    }

    public Category createOrUpdateCategory( Category categoryToAdd )
    {
       logger.debug("createOrUpdateCategory called");
       return categoryRepository.save (categoryToAdd);
    }

    public void removeCategory(Integer id)
    {
        logger.debug("deleteCategory called");
        categoryRepository.delete( id );
    }

}
