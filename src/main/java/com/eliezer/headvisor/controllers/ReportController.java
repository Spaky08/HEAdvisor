package com.eliezer.headvisor.controllers;

import com.eliezer.headvisor.model.Report;
import com.eliezer.headvisor.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @RequestMapping(method = RequestMethod.GET)
    public@ResponseBody
    List<Report> reportList() {
        return reportService.getReportList();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public @ResponseBody
    Report updateReport(@RequestBody Report report) {
        report = reportService.createOrUpdateReport(report);
        return report;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody void removeReport(@PathVariable Integer id) {
        reportService.removeReport(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Report getReport(@PathVariable Integer id) {
        return reportService.getReport(id);
    }
}
