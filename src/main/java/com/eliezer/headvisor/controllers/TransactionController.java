package com.eliezer.headvisor.controllers;

import com.eliezer.headvisor.model.Transaction;
import com.eliezer.headvisor.services.TransactionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    private Logger logger = Logger.getLogger( TransactionController.class );

    @RequestMapping(method = RequestMethod.GET)
    public@ResponseBody
    List<Transaction> transactionList() {
        return transactionService.getTransaction();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/byReport/{reportId}")
    public@ResponseBody
    List<Transaction> transactionListByReport(@PathVariable int reportId) {
        logger.debug("##EW: transactionListByReport: " + reportId);

        return transactionService.getTransactionsByReport(reportId);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public @ResponseBody
    Transaction updateTransactionMethod(@RequestBody Transaction transaction) {
        logger.debug("##EW: updateTransactionMethod: " + transaction.toString());

        transaction = transactionService.createOrUpdateTransaction(transaction);
        return transaction;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody void removeReportSection(@PathVariable Integer id) {
        transactionService.removeTransaction(id);
    }
}
