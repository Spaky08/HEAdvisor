package com.eliezer.headvisor.controllers;

import com.eliezer.headvisor.model.Category;
import com.eliezer.headvisor.model.SectionType;
import com.eliezer.headvisor.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Date: 12/10/13
 */
@Controller
@RequestMapping("/category")
public class CategoryController{

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(method = RequestMethod.GET)
    public@ResponseBody List<Category> categoryList() {
        return categoryService.getCategoryList();
    }

    @RequestMapping(value="/categoryTypes", method = RequestMethod.GET)
    public@ResponseBody List<HashMap<String, Object>> categoryTypeList() {
        List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
        retList.add(SectionType.INCOME.toMap());
        retList.add(SectionType.VAR.toMap());
        retList.add(SectionType.FIXED.toMap());
        retList.add(SectionType.ONE_TIME.toMap());
        return retList;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public @ResponseBody Category addCategory(@RequestBody Category category)
    {
        category =  categoryService.createOrUpdateCategory( category );
        return category;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody void removeCategory(@PathVariable Integer id)
    {
        categoryService.removeCategory(id);
    }
}
