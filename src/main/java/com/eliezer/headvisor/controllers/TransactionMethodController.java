package com.eliezer.headvisor.controllers;

import com.eliezer.headvisor.model.TransactionMethod;
import com.eliezer.headvisor.services.TransactionMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/transactionMethod")
public class TransactionMethodController {

    @Autowired
    private TransactionMethodService transactionMethodService;

    @RequestMapping(method = RequestMethod.GET)
    public@ResponseBody
    List<TransactionMethod> transactionMethodList() {
        return transactionMethodService.getTransactionMethods();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public @ResponseBody
    TransactionMethod updateTransactionMethod(@RequestBody TransactionMethod transactionMethod) {
        transactionMethod = transactionMethodService.createOrUpdateTransactionMethod(transactionMethod);
        return transactionMethod;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody void removeReportSection(@PathVariable Integer id) {
        transactionMethodService.removeTransactionMethod(id);
    }
}
