package com.eliezer.headvisor.controllers;

import com.eliezer.headvisor.model.ReportSection;
import com.eliezer.headvisor.services.ConfigReportSectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/configReportSection")
public class ConfigReportSectionController {
    @Autowired
    private ConfigReportSectionService configReportSectionService;

    @RequestMapping(method = RequestMethod.GET)
    public@ResponseBody
    List<ReportSection> reportSectionList() {
        return configReportSectionService.getReportSectionList();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public @ResponseBody
    ReportSection updateReportSection(@RequestBody ReportSection reportSection)
    {
        reportSection =  configReportSectionService.createOrUpdateReportSection(reportSection);
        return reportSection;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody void removeReportSection(@PathVariable Integer id) {
        configReportSectionService.removeReportSection(id);
    }
}
