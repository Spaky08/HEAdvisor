/**
 * Created with IntelliJ IDEA.
 * User: Eliezer
 * Date: 09/12/13
 * Time: 23:51
 * To change this template use File | Settings | File Templates.
 */
angular.module('hea-report').controller('FixedSectionController', ['$scope', function ($scope) {
    "use strict";

    $scope.section = {
        title : "Fixed Expenses"
    };

    function getNewTx() {
        return {
            category: "Fuel",
            estimated: 900,
            actual: 0,
            comments: "",
            moneyTransferType: "Credit Card"
        };
    }

    $scope.newTx = getNewTx();

    $scope.reportSection = {
        transactions: [{
            estimated: 500,
            actual: 477,
            category: "Electricity",
            moneyTransferType: "Credit Card",
            comments: ""
        }, {
            estimated: 32,
            actual: 0,
            category: "TV tax",
            moneyTransferType: "Credit Card",
            comments: "next month"
        }, {
            estimated: 900,
            actual: 731,
            category: "Fuel",
            moneyTransferType: "Credit Card",
            comments: "Yevul *2"
        }, {
            estimated: 900,
            actual: 212,
            category: "Fuel",
            moneyTransferType: "Credit Card",
            comments: ""
        }],
        categories: [
            "Electricity",
            "TV tax",
            "Fuel"
        ],
        moneyTransferTypes: [
            "Credit Card",
            "Bank Transaction",
            "Cash"
        ]
    };

    $scope.total = {
        estimated: 0,
        actual: 0
    };

    function calculateTotals() {
        $scope.total.estimated = $scope.totalEstimated($scope.reportSection.transactions);
        $scope.total.actual = $scope.totalActual($scope.reportSection.transactions);
    }

    $scope.showNewTransactionForm = function () {
        return true;
    };

    calculateTotals();

    $scope.calculateDifference = function () {
        return $scope.newTx.estimated - $scope.newTx.actual;
    };

    function getEstimatedForCategory(category) {
        var estimated = 0;
        switch (category) {
        case "Fuel":
            estimated = 900;
            break;
        case "TV tax":
            estimated = 32;
            break;
        case "Electricity":
            estimated = 500;
            break;
        default:
            break;
        }
        return estimated;
    }

    $scope.$watch('newTx.category', function (newCategory, oldCategory) {
        if ($scope.newTx === null) {
            $scope.newTx = getNewTx();
        }
        $scope.newTx.estimated = getEstimatedForCategory(newCategory);
    });
}]);