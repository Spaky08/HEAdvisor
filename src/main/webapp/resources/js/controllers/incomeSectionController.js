/**
 * Created with IntelliJ IDEA.
 * User: Eliezer
 * Date: 09/12/13
 * Time: 23:30
 * To change this template use File | Settings | File Templates.
 */
angular.module('hea-report').controller('IncomeSectionController', ['$scope', function ($scope) {
    "use strict";

    $scope.section = {
        title : "Income"
    };

    $scope.reportSection = {
        transactions: [{
            estimated: 15000,
            actual: 15321,
            category: "Salary",
            moneyTransferType: "Bank transaction",
            comments: ""
        }, {
            estimated: 173,
            actual: 173,
            category: "National Insurance",
            moneyTransferType: "Bank transaction",
            comments: "as usual"
        }]
    };

    $scope.total = {
        estimated: 0,
        actual: 0        
    }

    function calculateTotals () {
        $scope.total.estimated = $scope.totalEstimated($scope.reportSection.transactions);
        $scope.total.actual = $scope.totalActual($scope.reportSection.transactions);
    }

    calculateTotals();

}]);