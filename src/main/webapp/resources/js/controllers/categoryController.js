//noinspection JSHint
angular.module('myApp').controller('CategoryController', ['$scope', '$modal', 'Categories', 'ConfigReportSections', function ($scope, $modal, Categories, ConfigReportSections) {
    "use strict";
    var ADD_NEW_CATEGORY_TITLE = "Add New Category";
    var UPDATE_CATEGORY_TITLE = "Edit Category";

    $scope.data = {
        categories: [],
        reportSections: [],
        reportSectionsMap: [],
        orderBy: 'name',
        reverse: false
    };

    $scope.elementClass = {
        name: 'fa fa-sort-up',
        amount: 'fa fa-sort',
        type: 'fa fa-sort'
    };

    function getReportSectionById(id) {
        return $scope.data.reportSectionsMap[id];

    }

    function setReportSectionIfNumber () {
        for (var index = 0; index < $scope.data.categories.length; index++) {
            var category = $scope.data.categories[index];
            if (typeof category.reportSection === "number") {
                category.reportSection = getReportSectionById(category.reportSection);
            }
        }
    }

    function refreshList() {
        Categories.getCategoryResource.query(function (response) {
            $scope.data.categories = response;
            setReportSectionIfNumber();
        });
    }

    function initReportSectionMap() {
        $scope.data.reportSectionsMap = [];
        for (var index = 0; index < $scope.data.reportSections.length; index++) {
            var reportSection = $scope.data.reportSections[index];
            var id = reportSection['@id'];
            $scope.data.reportSectionsMap[id] = reportSection;
        }
    }

    function getReportSections() {
        ConfigReportSections.getConfigReportSectionResource.query(function (response) {
            $scope.data.reportSections = response;
            initReportSectionMap();
            refreshList();
        });
    }

    function init() {
        getReportSections();
    }

    init();

    // edit link
    $scope.editCategory = function (category) {
        var title = UPDATE_CATEGORY_TITLE;

        $scope.openEditCategoryModal(category, title);
    };


    // delete link
    $scope.deleteCategory = function (category) {
        Categories.getCategoryResource.remove(category, function () {
            refreshList();
        });
    };


    function resetSortClass() {
        $scope.elementClass.name = 'fa fa-sort';
        $scope.elementClass.amount = 'fa fa-sort';
        $scope.elementClass.type = 'fa fa-sort';
    }

    function flipSortClass(orderBy) {
        if (orderBy === "name") {
            $scope.elementClass.name = ($scope.elementClass.name === 'fa fa-sort-up') ? 'fa fa-sort-down' : 'fa fa-sort-up';
        } else if (orderBy === "type") {
            $scope.elementClass.type = ($scope.elementClass.type === 'fa fa-sort-up') ? 'fa fa-sort-down' : 'fa fa-sort-up';
        } else {
            $scope.elementClass.amount = ($scope.elementClass.amount === 'fa fa-sort-up') ? 'fa fa-sort-down' : 'fa fa-sort-up';
        }
    }

    $scope.setOrderBy = function (orderBy) {
        if (orderBy === $scope.data.orderBy) {
            $scope.data.reverse = !$scope.data.reverse;
        } else {
            $scope.data.reverse = false;
            resetSortClass();
        }
        flipSortClass(orderBy);
        $scope.data.orderBy = orderBy;
    };

    $scope.addNewCategory = function () {
        var emptyCategory = Categories.getEmptyCategory();

        var title = ADD_NEW_CATEGORY_TITLE;
        $scope.openEditCategoryModal(emptyCategory, title);
    };

    $scope.updateCallback = function(){
        refreshList();
    };

    $scope.openEditCategoryModal = function (category, title) {
        var modalInstance = $modal.open({
            templateUrl: "resources/view/category/editCategory.html",
            controller: 'EditCategory',
            resolve: {
                category: function () {
                    return category;
                },
                title: function () {
                    return title;
                },
                reportSections: function () {
                    return $scope.data.reportSections;
                },
                updateCallback: function () {
                    return $scope.updateCallback;
                }
            }
        });

        modalInstance.result.then(function () {
            refreshList();
        });
    };
}]);