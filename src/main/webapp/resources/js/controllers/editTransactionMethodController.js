angular.module('myApp').controller('EditTransactionMethod', ['$scope', '$modalInstance', 'TransactionMethods', 'transactionMethod', 'title', function ($scope, $modalInstance, TransactionMethods, transactionMethod, title) {
    "use strict";

    $scope.title = title;

    $scope.data = {
        tempTransactionMethod: angular.copy(transactionMethod),
        chosenTransactionMethod: transactionMethod
    };

    // reset button
    $scope.resetTransactionMethod = function () {
        $scope.data.tempTransactionMethod = angular.copy($scope.data.chosenTransactionMethod);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.saveChanges = function () {
        TransactionMethods.getTransactionMethodResource.save($scope.data.tempTransactionMethod, function () {
            $modalInstance.close($scope.data.tempTransactionMethod);
        });

    };

}]);