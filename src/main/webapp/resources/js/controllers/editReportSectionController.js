/**
 * Created by Eliezer on 16/04/2014.
 */
angular.module('myApp').controller('EditReportSection', ['$scope', '$modalInstance', '$http', 'ConfigReportSections', 'reportSection', 'title', function ($scope, $modalInstance, $http, ConfigReportSections, reportSection, title) {
    "use strict";

    $scope.title = title;

    $scope.data = {
        tempReportSection: angular.copy(reportSection),
        chosenReportSection: reportSection
    };

    // cancel button
    $scope.resetReportSection = function () {
        $scope.data.tempReportSection = angular.copy($scope.data.chosenReportSection);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.saveChanges = function () {
        ConfigReportSections.getConfigReportSectionResource.save($scope.data.tempReportSection, function (response) {
            $modalInstance.close($scope.data.tempReportSection);
        });

    };

}]);