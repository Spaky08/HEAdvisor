angular.module('myApp').controller('TransactionController', ['$scope', '$modal', '$routeParams',  'Transactions', 'Categories', 'TransactionMethods', 'Reports', 'ConfigReportSections', function ($scope, $modal, $routeParams, Transactions, Categories, TransactionMethods, Reports, ConfigReportSections) {
    "use strict";

    var UPDATE_TITLE = "Update Transaction";
    var ADD_TITLE = "Create a Transaction";

    $scope.data = {
        transactions: [],
        categories: [],
        transactionMethods: [],
        reportSections: [],
        transactionByReportSections: []
    };

    $scope.query = {
        reportId: $routeParams.reportId,
        before: $routeParams.before,
        report: {}
    };

    function getAllTransactions() {
        Transactions.getTransactionResource.query(function (response){
            $scope.data.transactions = response;
            prepareTransactionsForDisplay();
        });
    }

    function getTransactionByReport() {
        Transactions.getTransactionsByReport.get({
            reportId: $scope.query.reportId
            }, function (response){
                $scope.data.transactions = response;
                prepareTransactionsForDisplay();
            });
    }

    function prepareTransactionsForDisplay() {
        $scope.data.transactionByReportSections = Transactions.getTransactionByReportSections($scope.data.transactions, $scope.data.reportSections);
    }

    function refreshList() {
        if (!$scope.query.reportId || $scope.query.reportId === 0) {
            getAllTransactions();
        } else {
            getTransactionByReport();
        }
    }

    function getCategories() {
        Categories.getCategoryResource.query(function (response) {
            $scope.data.categories = response;
        });
    }

    function getReportSections() {
        ConfigReportSections.getConfigReportSectionResource.query(function(response) {
            var reportSections = response;
            var sortedReportSections = ConfigReportSections.sortReportSections(reportSections);
            $scope.data.reportSections = sortedReportSections;
            refreshList();
        });
    }

    function getTransactionMethods() {
        TransactionMethods.getTransactionMethodResource.query(function (response){
            $scope.data.transactionMethods = response;
        });
    }

    function getReport() {
        Reports.getReportsResource.get({id: $scope.query.reportId}, function(response) {

            $scope.query.report = {
                id: response.id,
                date: new Date (response.date),
                comments: response.comments
            };
        });
    }

    function init() {
        getReportSections();
        getReport();
        getCategories();
        getTransactionMethods();
        refreshList();
    }
    init();

    $scope.editTransaction = function (transaction) {
        $scope.openEditTransactionModal(transaction, UPDATE_TITLE);
    };

    $scope.addNewTransaction = function () {
        var emptyTransaction = Transactions.getEmptyTransaction($scope.query.report);
        $scope.openEditTransactionModal(emptyTransaction, ADD_TITLE);
    };

    $scope.deleteTransaction = function (transaction) {
        Transactions.getTransactionResource.remove(transaction, function () {
            refreshList();
        });
    };

    $scope.updateCallback = function() {
        refreshList();
    };

    $scope.openEditTransactionModal = function (transaction, title) {

        var modalInstance = $modal.open({
            templateUrl: "resources/view/transaction/editTransaction.html",
            controller: 'EditTransaction',
            resolve: {
                transaction: function () {
                    return transaction;
                },
                transactionMethods: function () {
                    return $scope.data.transactionMethods;
                },
                categories: function () {
                    return $scope.data.categories;
                },
                title: function () {
                    return title;
                },
                updateCallback: function (){
                    return $scope.updateCallback;
                }
            }
        });

        modalInstance.result.then(function () {
            refreshList();
        });
    };
}]);