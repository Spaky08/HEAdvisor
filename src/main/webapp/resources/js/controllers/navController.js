angular.module('hea-nav', []).controller('NavController', ['$scope', '$location', function ($scope, $location) {
    "use strict";

    $scope.getClass = function (path) {
        // will return true when the getClass called on the current view
        return path === $location.path().substr(0, path.length);
    };

}]);
