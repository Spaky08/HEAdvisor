angular.module('myApp').controller('EditTransaction', ['$scope', '$modalInstance', 'Transactions', 'transaction', 'transactionMethods', 'categories', 'title', 'updateCallback', function ($scope, $modalInstance, Transactions, transaction, transactionMethods, categories, title, updateCallback) {
    "use strict";

    $scope.title = title;

    $scope.data = {
        tempTransaction: angular.copy(transaction),
        chosenTransaction: transaction,
        transactionMethods: transactionMethods,
        categories: categories
    };

    $scope.report = {};

    // have it from the list so it will appear in the options list
    function normalizeTransactionCategory (transaction){
        var category = transaction.category.name;
        for (var index = 0; index < $scope.data.categories.length; index++) {
            var optionCategory = $scope.data.categories[index];
            if (category === optionCategory.name) {
                transaction.category = optionCategory;
                return transaction;
            }
        }

        return transaction;
    }

    // have it from the list so it will appear in the options list
    function normalizeTransactionTMethod (transaction){
        var transactionMethod = transaction.transactionMethod.name;
        for (var index = 0; index < $scope.data.transactionMethods.length; index++) {
            var optionTransactionMethod = $scope.data.transactionMethods[index];
            if (transactionMethod === optionTransactionMethod.name) {
                transaction.transactionMethod = optionTransactionMethod;
                return transaction;
            }
        }

        return transaction;
    }

    function init() {
        $scope.data.tempTransaction = normalizeTransactionCategory($scope.data.tempTransaction);
        $scope.data.tempTransaction = normalizeTransactionTMethod($scope.data.tempTransaction);
        $scope.report = $scope.data.tempTransaction.report;
    }

    init();
    // reset button
    $scope.resetTransaction = function () {
        $scope.data.tempTransaction = angular.copy($scope.data.chosenTransaction);
        $scope.data.tempTransaction = normalizeTransactionCategory($scope.data.tempTransaction);
        $scope.data.tempTransaction = normalizeTransactionTMethod($scope.data.tempTransaction);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.saveChanges = function () {
        Transactions.getTransactionResource.save($scope.data.tempTransaction, function () {
            $modalInstance.close($scope.data.tempTransaction);
        });

    };

    $scope.saveAndCreateNew = function() {
        Transactions.getTransactionResource.save($scope.data.tempTransaction, function () {
            $scope.data.chosenTransaction = Transactions.getEmptyTransaction($scope.report);
            $scope.resetTransaction();

        });

        updateCallback();

    };

    $scope.categoryLabel = function (category) {
        var categoryLabel = category.name + " (" + category.reportSection.name + ")";
        return categoryLabel;
    };

    $scope.$watch('data.tempTransaction.category', function (newCategory) {
        $scope.data.tempTransaction.predictedAmount = newCategory.amount;
    });

}]);