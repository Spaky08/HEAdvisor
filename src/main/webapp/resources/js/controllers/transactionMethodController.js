angular.module('myApp').controller('TransactionMethodController', ['$scope', '$modal', 'TransactionMethods', function ($scope, $modal, TransactionMethods) {
    "use strict";

    var UPDATE_TITLE = "Update Transaction Method";
    var ADD_TITLE = "Create a Transaction Method";

    $scope.data = {
        transactionMethods: []
    };

    function refreshList() {
        TransactionMethods.getTransactionMethodResource.query(function (response){
            $scope.data.transactionMethods = response;
        });
    }

    function getEmptyTransactionMethod() {
        return {
            id: 0,
            name: "",
            description: ""
        };
    }

    function init() {
        refreshList();
    }

    init();

    $scope.editTransactionMethod = function (transactionMethod) {
        $scope.openEditTransactionMethodModal(transactionMethod, UPDATE_TITLE);
    };

    $scope.addNewTransactionMethod = function () {
        var emptyTransactionMethod = getEmptyTransactionMethod();
        $scope.openEditTransactionMethodModal(emptyTransactionMethod, ADD_TITLE);
    };

    $scope.deleteTransactionMethod = function (transactionMethod) {
        TransactionMethods.getTransactionMethodResource.remove(transactionMethod, function () {
            refreshList();
        });
    };

    $scope.openEditTransactionMethodModal = function (transactionMethod, title) {

        var modalInstance = $modal.open({
            templateUrl: "resources/view/transaction/editTransactionMethod.html",
            controller: 'EditTransactionMethod',
            resolve: {
                transactionMethod: function () {
                    return transactionMethod;
                },
                title: function () {
                    return title;
                }
            }
        });

        modalInstance.result.then(function () {
            refreshList();
        });
    };
}]);