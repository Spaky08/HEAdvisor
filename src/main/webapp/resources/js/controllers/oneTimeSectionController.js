/**
 * Created with IntelliJ IDEA.
 * User: Eliezer
 * Date: 10/12/13
 * Time: 00:01
 * To change this template use File | Settings | File Templates.
 */
angular.module('hea-report').controller('OneTimeSectionController', ['$scope', function ($scope) {
    "use strict";

    $scope.section = {
        title : "Other Expenses"
    };

    $scope.reportSection = {
        transactions: [{
            estimated: 853,
            actual: 853,
            category: "Car - Maintenance",
            moneyTransferType: "Credit Card",
            comments: "battery"
        }, {
            estimated: 673,
            actual: 673,
            category: "Aviezer",
            moneyTransferType: "Credit Card",
            comments: "Advertising - for Moaaza"
        }]
    };

    $scope.total = {
        estimated: 0,
        actual: 0        
    }

    function calculateTotals () {
        $scope.total.estimated = $scope.totalEstimated($scope.reportSection.transactions);
        $scope.total.actual = $scope.totalActual($scope.reportSection.transactions);
    }

    calculateTotals();

}]);