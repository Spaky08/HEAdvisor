angular.module('hea-report', []).controller('ReportController', ['$scope', function ($scope) {
    "use strict";
    $scope.report = {
        date: new Date()
    };

    $scope.template = {
        url: 'resources/view/report/reportSectionView.tpl.html'
    };

    $scope.editTransaction = function (tx) {
        console.log('editTransaction');
    };

    $scope.deleteTransaction = function (tx) {
        console.log('deleteTransaction');
    };

    $scope.totalEstimated = function (transactions) {
        var index = 0;
        var totalEstimated = 0;
        var categoriesTakenToEstimated = [];
        for (index; index < transactions.length; index++) {
            var tx = transactions[index];
            if (!categoriesTakenToEstimated[tx.category]) {
                totalEstimated += tx.estimated;
                categoriesTakenToEstimated[tx.category] = true;
            }
        }

        return totalEstimated;
    };

    $scope.totalActual = function (transactions) {
        var index = 0;
        var totalActual = 0;
        for (index; index < transactions.length; index++) {
            var tx = transactions[index];
            totalActual += tx.actual;
        }

        return totalActual;
    };

}]);