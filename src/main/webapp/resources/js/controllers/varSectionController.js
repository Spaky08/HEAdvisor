/**
 * Created with IntelliJ IDEA.
 * User: Eliezer
 * Date: 09/12/13
 * Time: 23:57
 * To change this template use File | Settings | File Templates.
 */
angular.module('hea-report').controller('VarSectionController', ['$scope', function ($scope) {
    "use strict";

    $scope.section = {
        title : "Varied Expenses"
    };

    $scope.reportSection = {
        transactions: [{
            estimated: 2000,
            actual: 2000,
            category: "Food",
            moneyTransferType: "Cash",
            comments: ""
        }, {
            estimated: 150,
            actual: 112,
            category: "Entertainment",
            moneyTransferType: "Cash",
            comments: "Movie"
        }]
    };
    

    $scope.total = {
        estimated: 0,
        actual: 0        
    }

    function calculateTotals () {
        $scope.total.estimated = $scope.totalEstimated($scope.reportSection.transactions);
        $scope.total.actual = $scope.totalActual($scope.reportSection.transactions);
    }

    calculateTotals();

}]);