angular.module('hea-reports', []).controller('ReportsController', ['$scope', '$modal', 'Reports', function ($scope, $modal, Reports) {
    "use strict";

    var today = new Date();

    var ADD_NEW_REPORT = "Add New Report";
    var EDIT_REPORT = "Edit Report";

    $scope.data = {
        reports: []
    };

    $scope.getEmptyReport = function () {
        return {
            id: 0,
            date: today,
            comments: ""
        };
    };

    $scope.convertDateIntToObjects = function (reports) {
        var index;
        for (index = 0; index < reports.length; index++) {
            reports[index].date = new Date(reports[index].date);
        }
        return reports;
    };

    function refreshList() {
        Reports.getReportsResource.query(function (response) {
            var reports = response;
            reports = $scope.convertDateIntToObjects (reports);
            $scope.data.reports = reports;
        });
    }

    function init() {
        refreshList();
    }

    init();

    $scope.getTransactionLink = function (report) {
        return "#/transactions/" + report.id + "/" + report.date.valueOf();
    };

    $scope.viewReport = function (report) {
        console.log('viewReport' + report.date);
    };

    $scope.editReport = function (report) {
        report.date = new Date(report.date);

        $scope.openEditReportModal(report, EDIT_REPORT);
    };

    $scope.deleteReport = function (report) {
        Reports.getReportsResource.remove(report, function () {
            refreshList();
        });
    };

    $scope.addNewReport = function () {
        var emptyReport = $scope.getEmptyReport();

        $scope.openEditReportModal(emptyReport, ADD_NEW_REPORT);
    };

    $scope.openEditReportModal = function (report, title) {

        var modalInstance = $modal.open({
            templateUrl: "resources/view/report/editReport.html",
            controller: 'EditReport',
            resolve: {
                report: function () {
                    return report;
                },
                title: function () {
                    return title;
                }
            }
        });

        modalInstance.result.then(function () {
            refreshList();
        });
    };
}]);