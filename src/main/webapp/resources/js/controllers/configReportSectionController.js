angular.module('myApp').controller('ConfigReportSectionController', ['$scope', '$modal', 'ConfigReportSections', function ($scope, $modal, ConfigReportSections) {
    "use strict";
    var UPDATE_TITLE = "Update Report Section";
    var ADD_TITLE = "Create a Report Section";

    $scope.data = {
        reportSections: []
    };

    function refreshList() {
        ConfigReportSections.getConfigReportSectionResource.query(function (response) {
            $scope.data.reportSections = response;
        });
    }

    function getEmptyReportSection() {
        var emptyReportSection;
        emptyReportSection = {
            id: 0,
            name: "",
            description: "",
            order: $scope.data.reportSections.length + 1
        };
        return emptyReportSection;
    }

    function init() {
        refreshList();
    }

    init();

    // edit link
    $scope.editReportSection = function (reportSection) {
        var title = UPDATE_TITLE;

        $scope.openAddReportSectionModal(reportSection, title);
    };

    $scope.addNewReportSection = function () {
        var emptyReportSection = getEmptyReportSection();
        var title = ADD_TITLE;
        $scope.openAddReportSectionModal(emptyReportSection, title);
    };

    $scope.openAddReportSectionModal = function (inputReportSection, title) {

        var modalInstance = $modal.open({
            templateUrl: "resources/view/configuration/reportSection/editReportSection.html",
            controller: 'EditReportSection',
            resolve: {
                reportSection: function () {
                    return inputReportSection;
                },
                title: function () {
                    return title;
                }
            }
        });

        modalInstance.result.then(function () {
            refreshList();
        });
    };

    // delete link
    $scope.deleteReportSection = function (reportSection) {
        ConfigReportSections.getConfigReportSectionResource.remove(reportSection, function () {
            refreshList();
        });
    };
}]);


