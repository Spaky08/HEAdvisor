angular.module('myApp').controller('EditCategory', ['$scope', '$modalInstance', 'Categories', 'category', 'title', 'reportSections', 'updateCallback', function ($scope, $modalInstance, Categories, category, title, reportSections, updateCallback) {
    "use strict";

    $scope.title = title;

    $scope.data = {
        reportSections: reportSections,
        tempCategory: angular.copy(category),
        chosenCategory: category
    };

    // have it from the list so it will appear in the options list
    function normalizeCategoryReportSection (category){
        var reportSectionName = category.reportSection.name;
        for (var index = 0; index < $scope.data.reportSections.length; index++) {
            var optionReportSection = $scope.data.reportSections[index];
            if (reportSectionName === optionReportSection.name) {
                category.reportSection = optionReportSection;
                return category;
            }
        }

        return category;
    }

    function init() {
        $scope.data.tempCategory = normalizeCategoryReportSection($scope.data.tempCategory);
    }

    init();

    // cancel button
    $scope.resetCategory = function () {
        $scope.data.tempCategory = angular.copy($scope.data.chosenCategory);
        $scope.data.tempCategory = normalizeCategoryReportSection($scope.data.tempCategory);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.saveChanges = function () {
        Categories.getCategoryResource.save($scope.data.tempCategory, function () {
            $modalInstance.close($scope.data.tempCategory);
        });
    };

    $scope.saveAndCreateNew = function() {
        Categories.getCategoryResource.save($scope.data.tempCategory, function () {
            $scope.data.chosenCategory = Categories.getEmptyCategory();
            $scope.resetCategory();
            updateCallback();
        });
    };

}]);