angular.module('myApp').controller('EditReport', ['$scope', '$modalInstance', 'Reports', 'report', 'title', function ($scope, $modalInstance, Reports, report, title) {
    "use strict";

    $scope.title = title;

    $scope.dateToString = function (report) {
        report.date = report.date.toString('MM/yyyy');

        return report;
    };

    $scope.stringToDate = function (report) {
        var date = report.date;

        if (typeof date === "string") {
            report.date = Date.parseExact(date, ["M/yyyy", "MM/yyyy", "M-yyyy", "MM-yyyy"]);
        }
        return report;
    };

    $scope.report = $scope.dateToString(report);

    $scope.data = {
        tempReport: angular.copy($scope.report),
        chosenReport: $scope.report
    };

    $scope.date = {
        format: 'MM/yyyy'
    };

    // cancel button
    $scope.resetReport = function () {
        $scope.data.tempReport = angular.copy($scope.data.chosenReport);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.saveChanges = function () {
        var reportToSave = $scope.stringToDate($scope.data.tempReport);

        Reports.getReportsResource.save(reportToSave, function () {
            $modalInstance.close(reportToSave);
        });
    };

}]);