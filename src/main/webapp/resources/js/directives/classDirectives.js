/**
 * Created by Eliezer on 18/04/2014.
 */
angular.module('myApp').directive('hoverClass', function () {
    "use strict";

    return {
        restrict: 'A',
        scope: {
            hoverClass: '@'
        },
        link: function (scope, element) {
            element.on('mouseenter', function () {
                element.addClass(scope.hoverClass);
            });
            element.on('mouseleave', function () {
                element.removeClass(scope.hoverClass);
            });
        }
    };
}).directive('mouseLeaveClass', function () {
    "use strict";

    return {
        restrict: 'A',
        scope: {
            mouseLeaveClass: '@'
        },
        link: function (scope, element) {
            element.on('mouseleave', function () {
                element.addClass(scope.hoverClass);
            });
            element.on('mouseenter', function () {
                element.removeClass(scope.mouseLeaveClass);
            });
        }
    };
});