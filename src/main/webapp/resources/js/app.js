/**
 * Created with IntelliJ IDEA.
 * User: Eliezer
 * Date: 27/09/13
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */
// Declare app level module which depends on filters, and services
angular.module('myApp', ['ngResource', 'ngRoute', 'hea-nav', 'hea-reports', 'hea-report', 'ui.bootstrap']).
    config(['$routeProvider', function ($routeProvider) {
        'use strict';
        $routeProvider.when('/categories', {templateUrl: 'resources/view/category/categoryList.html', controller: 'CategoryController'});
        $routeProvider.when('/reports', {templateUrl: 'resources/view/report/reportsList.html', controller: 'ReportsController'});
        $routeProvider.when('/report/:id', {templateUrl: 'resources/view/report/reportView.html', controller: 'ReportController'});
        $routeProvider.when('/reportSections', {templateUrl: 'resources/view/configuration/reportSection/reportSectionList.html', controller: 'ConfigReportSectionController'});
        $routeProvider.when('/transactionMethods', {templateUrl: 'resources/view/transaction/transactionMethodList.html', controller: 'TransactionMethodController'});
        $routeProvider.when('/transactions', {templateUrl: 'resources/view/transaction/transactionList.html', controller: 'TransactionController'});
        $routeProvider.when('/transactions/:reportId/:before', {templateUrl: 'resources/view/transaction/transactionList.html', controller: 'TransactionController'});
        $routeProvider.otherwise({redirectTo: '/reports'});
    }]);
