angular.module('myApp')
    .factory('Reports', function ($resource) {

        "use strict";
        var factory = {};

        factory.getReportsResource = $resource('http://localhost:8080/report/:id', {id: '@id'}, {
            'delete': {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        });

        return factory;
    })
    .value('version', '0.1');