angular.module('myApp')
    .factory('Transactions', ['$resource', 'Categories', 'TransactionMethods', function ($resource, Categories, TransactionMethods) {

        "use strict";
        var factory = {};

        factory.getTransactionResource = $resource('http://localhost:8080/transaction/:id', {id: '@id'}, {
            'delete': {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        });

        factory.getTransactionsByReport = $resource('http://localhost:8080/transaction/byReport/:reportId',{reportId: '@reportId'}, {
            'get': {method: 'GET', isArray:true}
        });

        factory.getEmptyTransaction = function (report) {
            return {
                id: 0,
                predictedAmount: 0,
                actualAmount: 0,
                comments: "",
                category: Categories.getDefaultCategory(),
                transactionMethod: TransactionMethods.getDefaultTransactionMethod(),
                report: report
            };
        }


        function initTransactionByReportSection(reportSections, transactionByReportSection) {
            for (var index = 0; index < reportSections.length; index++) {
                var reportSection = reportSections[index];
                transactionByReportSection[reportSection.name] = [];
            }
            return transactionByReportSection;
        }

        function insertTransaction(transaction, transactionByReportSection) {
            var transactionReportSection = transaction.category.reportSection.name;

            if (transactionByReportSection[transactionReportSection]) {
                transactionByReportSection[transactionReportSection].push(transaction);
            } else {
                console.log("getTransactionByReportSections - unknown report section " + transactionReportSection);
            }

            return transactionByReportSection;
        }

        var calculateBottomLineForTransactions = function (transactions) {
            var countedCategories = [];
            var actual = 0;
            var predicted = 0;

            for (var index = 0; index < transactions.length; index++) {
                var transaction = transactions[index];
                if (!countedCategories[transaction.category.name]) {
                    countedCategories[transaction.category.name] = true;
                    predicted += transaction.category.amount;
                }
                actual += transaction.actualAmount;
            }
            return {predicted: predicted,
                    actual: actual};
        };

        function createTrasactionsGroupedByReportSection(reportSections, transactionByReportSection) {
            var sortedTransactionByReportSection = [];
            for (var index = 0; index < reportSections.length; index++) {
                var reportSectionName = reportSections[index].name;
                sortedTransactionByReportSection.push({
                    reportSection: reportSectionName,
                    transactions: transactionByReportSection[reportSectionName],
                    total: calculateBottomLineForTransactions(transactionByReportSection[reportSectionName])
                });
            }
            return sortedTransactionByReportSection;
        }

        factory.getTransactionByReportSections = function (transactions, reportSections) {
            var transactionByReportSection = [];

            transactionByReportSection = initTransactionByReportSection(reportSections, transactionByReportSection);

            for (var transactionIndex = 0; transactionIndex < transactions.length; transactionIndex++) {
                var transaction = transactions[transactionIndex];
                transactionByReportSection = insertTransaction(transaction, transactionByReportSection);
            }

            return createTrasactionsGroupedByReportSection(reportSections, transactionByReportSection);
        };

        return factory;
    }])
    .value('version', '0.1');