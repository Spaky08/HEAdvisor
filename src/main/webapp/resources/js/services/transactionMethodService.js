angular.module('myApp')
    .factory('TransactionMethods', function ($resource) {

        "use strict";
        var factory = {};

        factory.getTransactionMethodResource = $resource('http://localhost:8080/transactionMethod/:id', {id: '@id'}, {
            'delete': {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        });

        factory.transactionMethods = [];

        function init() {
            factory.getTransactionMethodResource.query(function (response) {
                factory.transactionMethods = response;
            });
        }
        init();

        factory.getDefaultTransactionMethod = function() {
            if (factory.transactionMethods.length > 0) {
                return factory.transactionMethods[0];
            }
            return {};
        };

        return factory;
    })
    .value('version', '0.1');