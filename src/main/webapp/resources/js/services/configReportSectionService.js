angular.module('myApp')
    .factory('ConfigReportSections', function ($resource) {

        "use strict";
        var factory = {};

        factory.getConfigReportSectionResource = $resource('http://localhost:8080/configReportSection/:id', {id: '@id'}, {
            'delete': {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        });

        factory.reportSections = [];

        function init() {
            factory.getConfigReportSectionResource.query(function (response) {
                factory.reportSections = response;
            });
        }
        init();


        factory.sortReportSections = function (reportSections) {
            var sortedReportSections = reportSections.sort(compareReportSections);
            return sortedReportSections;
        };

        factory.getDefaultReportSection = function() {
            if (factory.reportSections.length > 0) {
                return factory.reportSections[0];
            }
            return {};
        };

        function compareReportSections(a,b) {
            if (a.order < b.order) {
                return -1;
            }
            if (a.order > b.order) {
                return 1;
            }
            return 0;
        }

        return factory;
    })
    .value('version', '0.1');