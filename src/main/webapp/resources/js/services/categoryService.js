angular.module('myApp')
    .factory('Categories', ['$resource', 'ConfigReportSections' , function ($resource, ConfigReportSections) {

        "use strict";
        var factory = {};

        factory.getCategoryResource = $resource('http://localhost:8080/category/:id', {id: '@id'}, {
            'delete': {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        });

        factory.categories = [];

        function init() {
            factory.getCategoryResource.query(function (response) {
                factory.categories = response;
            });
        }
        init();

        factory.getEmptyCategory= function () {
            return {
                id: 0,
                name: "",
                reportSection: ConfigReportSections.getDefaultReportSection(),
                amount: 0
            };
        };

        factory.getDefaultCategory = function () {
            if (factory.categories.length > 0) {
                return factory.categories[0];
            }
            return factory.getEmptyCategory();
        }

        return factory;
    }])
    .value('version', '0.1');

