<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Home Economy Adviser</title>
    <meta name="description" content="Home economic helper application">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<spring:url value="/resources/lib/boilerPlate/css/normalize.css" />">
    <link rel="stylesheet" href="<spring:url value="/resources/lib/boilerPlate/css/main.css" />">
    <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap/css/bootstrap.css" />">
<%--    <link rel="stylesheet" href="<spring:url value="/resources/lib/bootstrap/css/bootstrap-responsive.css" />">--%>
    <link rel="stylesheet" href="<spring:url value="/resources/lib/font-awesome/css/font-awesome.css" />">
    <link rel="stylesheet" href="<spring:url value="/resources/css/app.css" />">
    <script src="<spring:url value="/resources/lib/boilerPlate/js/vendor/modernizr-2.6.2.min.js" />"></script>
</head>
<html ng-app="myApp">
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
         <div class="container">
             <div class="navbar-header">
                 <a class="navbar-brand" href="#">Home Economy Adviser</a>
             </div>

            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav" role="navigation" data-ng-controller="NavController">
                    <li data-ng-class="{'active':getClass('/reports')}">
                        <a data-ng-href="#/reports">Reports</a>
                    </li>
                    <li class="dropdown-menu inverse-dropdown" >
                        <a class="dropdown-toggle" data-toggle="dropdown" ng-href="#">
                            Admin
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu inverse-dropdown">
                            <li data-ng-class="{'active':getClass('/categories')}">
                                <a data-ng-href="#/categories">Categories</a>
                            </li>
                            <li data-ng-class="{'active':getClass('/report sections')}">
                                <a data-ng-href="#/reportSections">Report Sections</a>
                            </li>
                            <li data-ng-class="{'active':getClass('/report sections')}">
                                <a data-ng-href="#/transactionMethods">Transaction Methods</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </div>


    <div class="container mainContainer" ng-view>
    </div>

    <%--External lib--%>
    <script src="<spring:url value="/resources/lib/jQuery/jquery-1.10.2.min.js" />"></script>
    <script src="<spring:url value="/resources/lib/boilerPlate/js/plugins.js" />"></script>
    <script src="<spring:url value="/resources/lib/boilerPlate/js/main.js" />"></script>
    <script src="<spring:url value="/resources/lib/bootstrap/js/bootstrap.js" />"></script>
    <%--Angular--%>
    <script src="<spring:url value="/resources/lib/angular/angular.js" />"></script>
    <script src="<spring:url value="/resources/lib/angular/angular-resource.js" />"></script>
    <script src="<spring:url value="/resources/lib/angular/angular-route.js" />"></script>
    <script src="<spring:url value="/resources/lib/ui-bootstrap/ui-bootstrap-tpls-0.10.0.min.js" />"></script>
    <script src="<spring:url value="/resources/lib/angular/angular-locale_he-il.js" />"></script>
    <script src="<spring:url value="/resources/lib/datejs/date.js" />"></script>
    <%--app--%>
    <script src="<spring:url value="/resources/js/app.js" />"></script>
    <%--Services--%>
    <script src="<spring:url value="/resources/js/services/categoryService.js" />"></script>
    <script src="<spring:url value="/resources/js/services/configReportSectionService.js" />"></script>
    <script src="<spring:url value="/resources/js/services/reportService.js" />"></script>
    <script src="<spring:url value="/resources/js/services/transactionMethodService.js" />"></script>
    <script src="<spring:url value="/resources/js/services/transactionService.js" />"></script>
    <%--Directives--%>
    <script src="<spring:url value="/resources/js/directives/classDirectives.js" />"></script>
    <%--Controllers--%>
    <script src="<spring:url value="/resources/js/controllers/navController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/categoryController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/editCategoryController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/configReportSectionController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/transactionMethodController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/editTransactionMethodController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/transactionListController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/editTransactionController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/reportsController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/editReportController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/reportController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/editReportSectionController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/incomeSectionController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/fixedSectionController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/varSectionController.js" />"></script>
    <script src="<spring:url value="/resources/js/controllers/oneTimeSectionController.js" />"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
        (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
</body>
</html>